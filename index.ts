const createJump = task('forest', (payload) => {
    const {foo} = payload;
    console.log(`Encountered jump with payload foo=${foo}`)
});


schedule('* * * * *', () => {
    console.log('Schedule triggered');
    createJump({foo: 3})
});
http('/foobar', (req, res) => res.send('hello world'));
